/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import java.util.ArrayList;

/**
 *
 * @author Tanvi Gurav
 */
public class AirlineDirectory {
    
    private ArrayList<Airliner> airlineList;
    

    public ArrayList<Airliner> getAirlineList() {
        return airlineList;
    }

    public void setAirlineList(ArrayList<Airliner> airlineList) {
        this.airlineList = airlineList;
    }
    
    
}
